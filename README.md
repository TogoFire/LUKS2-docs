![LUKS logo](https://gitlab.com/cryptsetup/cryptsetup/wikis/luks-logo.png)

LUKS2 format documentation
==========================
This repository contains work-in-progress documentation of LUKS2 format.

[PDF version](https://gitlab.com/cryptsetup/LUKS2-docs/blob/master/luks2_doc_wip.pdf).

LUKS2 reference implementation
==============================
Please see [cryptsetup](https://gitlab.com/cryptsetup/cryptsetup) project site.
